//
// Created by Iden on 10/1/2019.
//

#ifndef SSD_VIDEO_WEBCAMVIDEOSTREAM_H
#define SSD_VIDEO_WEBCAMVIDEOSTREAM_H

#include <thread>
#include <mutex>
#include <opencv2/videoio.hpp>

class WebcamVideoStream {
public:
    cv::VideoCapture stream;
    cv::Mat frame;
    bool stopped;
    std::mutex m;

    explicit WebcamVideoStream(const std::string &source);

    explicit WebcamVideoStream(int source);

    explicit WebcamVideoStream();

    void update();

    void start();

    cv::Mat read();

    void release();

};


#endif //SSD_VIDEO_WEBCAMVIDEOSTREAM_H
