//
// Created by Iden on 10/1/2019.
//
#include "WebcamVideoStream.h"

WebcamVideoStream::WebcamVideoStream(const std::string& source) {
    stream = cv::VideoCapture(source);
    stream >> frame;
    stopped = false;
}

WebcamVideoStream::WebcamVideoStream(int source) {
    stream = cv::VideoCapture(source);
    stream >> frame;
    stopped = false;
}

WebcamVideoStream::WebcamVideoStream() {
    stopped = false;
}

void WebcamVideoStream::update() {
    cv::Mat f;
    while (true) {
        if (stopped)
            return;
		stream >> f;
		m.lock();
        frame = f.clone();
		m.unlock();
    }
}

void WebcamVideoStream::start() {
    std::thread thread_object(&WebcamVideoStream::update, this);
}

cv::Mat WebcamVideoStream::read() {
    m.lock();
	cv::Mat f = frame.clone();
    m.unlock();
	return f;
}

void WebcamVideoStream::release() {
    stopped = true;
}

