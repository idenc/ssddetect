// this program implements the detection using a SSD RTM
#include <chrono>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <iostream>

/*
 * POSIX MMAP for Windows, optional but this example is written to memory map
 * the RT Models as opposed to reading them directly into memory.
 */
#ifdef _WIN32

#include <Windows.h>
#include <io.h>
#include "mman.h"

#else

#include <sys/mman.h>
#include <unistd.h>

#endif


#include "deepview_ops.h"
#include "deepview_rt.h"
#include "getopt.h"

#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include "WebcamVideoStream.h"

#define USAGE \
    "\
\n\
USAGE: \n\
       detect [hvV] \n\
       detect [-m mobilenetSSD_rtm_file] [-i sample image] <-c class_id> <-t threshold> <-n num_boxes>\n\
       -h, --help\n\
          \tDisplay help information\n\
       -v, --version\n\
       -V, --verbose\n\
          \tDisplay the verbose information\n\
          \tDisplay version information\n\
       -i, --input\n\
          \tInput testing image file \n\
       -m, --model\n\
          \tInput RTM model\n\
       -t, --threshold \n\
          \tThreshold for valid scores, by default it is set to -0.5\n\
       -s, --section \n\
          \tIOU threshold for NMS, by default it is set to 0.82\n\
       -c, --class \n\
          \tClass indices (model specific, i.e.,COCO dataset labels 1-91) \n\
       -n, --number \n\
          \tNumer of maximum predictions (bounding boxes)\n\
\n\
       \e[4mSample command:\e[0m\n\
       ./detect -m DATA_PATH/ssd_model.rtm -i DATA_PATH/sample.png -c 1 -s 0.82 \n\
\n\
       \e[4mSample command:\e[0m\n\
       ./detect -m DATA_PATH/ssd_model.rtm -i DATA_PATH/sample.png -c 1 18 -s 0.9 \n\
\n\
       \e[4mNotes:\e[0m\n\
       1. Sample data is located in smb://auzonenas01.local/share/Zhe/data\n\
       2. Output bounding box parameters are [ymin, xmin, ymax, xmax] normalized by input image width/height\n\
\n\
"

#ifdef _MSC_VER
__declspec(align(64)) static uint8_t cache[4 * 1024 * 1024];
__declspec(align(64)) static uint8_t pool[32 * 1024 * 1024];
#else
static uint8_t cache[4 * 1024 * 1024] __attribute((aligned(64)));
static uint8_t pool[32 * 1024 * 1024] __attribute((aligned(64)));
#endif


typedef struct ValIndTag {
    float val;
    int id;
} stValInd;

int compare(const void *a, const void *b) {
    return (((*(stValInd *) a).val - (*(stValInd *) b).val) > 0 ? 1 : -1);
}


int ssd_bbx_decode(float *bbx_params, float *bbx_anchor_coords,
                   int candidate_num, float *pred_bbx) {
    float ya = 0, xa = 0, wa = 0, ha = 0;
    float tx = 0, ty = 0, tw = 0, th = 0;
    float w = 0, h = 0, yc = 0, xc = 0;

    if (candidate_num < 0)
        return 1;

    for (int i = 0; i < candidate_num; i++) {
        ya = (bbx_anchor_coords[4 * i] + bbx_anchor_coords[4 * i + 2]) / 2.0;
        xa = (bbx_anchor_coords[4 * i + 1] + bbx_anchor_coords[4 * i + 3]) / 2.0;
        wa = bbx_anchor_coords[4 * i + 3] - bbx_anchor_coords[4 * i + 1];
        ha = bbx_anchor_coords[4 * i + 2] - bbx_anchor_coords[4 * i];

        ty = bbx_params[4 * i] / 10.0;
        tx = bbx_params[4 * i + 1] / 10.0;
        th = bbx_params[4 * i + 2] / 5.0;
        tw = bbx_params[4 * i + 3] / 5.0;

        w = expf(tw) * wa;
        h = expf(th) * ha;

        yc = ty * ha + ya;
        xc = tx * wa + xa;

        pred_bbx[4 * i] = (yc - h / 2.0);
        pred_bbx[4 * i + 1] = (xc - w / 2.0);
        pred_bbx[4 * i + 2] = (yc + h / 2.0);
        pred_bbx[4 * i + 3] = (xc + w / 2.0);

    }


    return 0;
}


/**
 * @brief This function sort the score tensor, output the indices tensor and
 *      indices dim tensors
 *
 * @param score_tensor             input of 1 x BBX_NUM x FULL_CLASS_NUM x 1
 * @param cache                    input
 * @param class_ind_tensor         input of 1 x REQ_CLASS_NUM
 * @param score_threshold_tensor   input of 1 x REQ_CLASS_NUM
 * @param max_ind_tensor           input of 1 x 1
 * @param indices_tensor           output of REQ_CLASS_NUM x BBX_NUM
 * @param indices_len_tensor       output of REQ_CLASS_NUM x 1
 *
 * @return
 **/
NN_API NNError
nn_ssd_sort_scores(NNTensor *score_tensor,
                   NNTensor *cache,
                   NNTensor *class_ind_tensor,
                   NNTensor *score_threshold_tensor,
                   NNTensor *max_ind_tensor,
                   NNTensor *indices_tensor,
                   NNTensor *indices_len_tensor);


/**
 * @brief  Decoding the corresponding bounding boxes
 *
 * @param trans                 input of 1 x MODEL_BBX_NUM x 1 x 4
 * @param anchor                input of MODEL_BBX_NUM x 4
 * @param cache
 * @param indices_tensor        input of REQ_CLASS_NUM x BBX_NUM
 * @param indices_len_tensor    input of REQ_CLASS_NUM x 1
 * @param bbx_candidates        output of REQ_CLASS_NUM x BBX_NUM x 4
 *
 * @return
 **/

NN_API NNError
nn_ssd_decode_bounding_box(NNTensor *trans,
                           NNTensor *anchor,
                           NNTensor *cache,
                           NNTensor *indices_tensor,
                           NNTensor *indices_len_tensor,
                           NNTensor *bbx_candidates);


NNError nn_ssd_decode_bounding_box(NNTensor *trans, NNTensor *anchor, NNTensor *cache, NNTensor *indices_tensor,
                                   NNTensor *indices_len_tensor, NNTensor *bbx_candidates) {

    NNError err;
    const int32_t *indices_len_shape = nn_tensor_shape(indices_len_tensor);
    const int32_t *trans_shape = nn_tensor_shape(trans);
    int32_t cache_size = nn_tensor_size(cache);
    float *cache_data = (float *) nn_tensor_maprw(cache);

    // check cache size
    float *bbx_params = (float *) cache_data;
    float *bbx_anchor_coords = (float *) ((float *) cache_data + 4 * 4 * trans_shape[1]);

    float *data_trans = (float *) nn_tensor_mapro(trans);
    float *data_anchor = (float *) nn_tensor_mapro(anchor);

    int32_t *indices_data = (int32_t *) nn_tensor_maprw(indices_tensor);
    if (!indices_data) return NN_ERROR_TENSOR_NO_DATA;

    int32_t *indices_len_data = (int32_t *) nn_tensor_maprw(indices_len_tensor);
    if (!indices_len_data) return NN_ERROR_TENSOR_NO_DATA;

    uint8_t bbx_per_class_mem[NN_TENSOR_SIZEOF];
    NNTensor *bbx_per_class = nn_tensor_init(bbx_per_class_mem, nn_tensor_engine(cache));
    int32_t bbx_per_class_shape[3] = {1, trans_shape[1], 4};
    float *bbx_per_class_data = NULL;

    // per class decoding
    for (int k = 0; k < indices_len_shape[0]; k++) {
        for (int j = 0; j < indices_len_data[nn_tensor_offsetv(indices_len_tensor, 2, k, 0)]; j++) {
            for (int i = 0; i < trans_shape[1]; i++) {
                if (i == indices_data[nn_tensor_offsetv(indices_tensor, 2, k, j)]) {
                    bbx_params[4 * j] = data_trans[nn_tensor_offsetv(trans, 4, 0, i, 0, 0)];
                    bbx_params[4 * j + 1] = data_trans[nn_tensor_offsetv(trans, 4, 0, i, 0, 1)];
                    bbx_params[4 * j + 2] = data_trans[nn_tensor_offsetv(trans, 4, 0, i, 0, 2)];
                    bbx_params[4 * j + 3] = data_trans[nn_tensor_offsetv(trans, 4, 0, i, 0, 3)];

                    bbx_anchor_coords[4 * j] = data_anchor[nn_tensor_offsetv(anchor, 2, i, 0)];
                    bbx_anchor_coords[4 * j + 1] = data_anchor[nn_tensor_offsetv(anchor, 2, i, 1)];
                    bbx_anchor_coords[4 * j + 2] = data_anchor[nn_tensor_offsetv(anchor, 2, i, 2)];
                    bbx_anchor_coords[4 * j + 3] = data_anchor[nn_tensor_offsetv(anchor, 2, i, 3)];

                    break;
                }
            }
        }

        err = nn_tensor_view(bbx_per_class, nn_tensor_type(bbx_candidates),
                             3, bbx_per_class_shape, bbx_candidates, sizeof(float) * 4 * trans_shape[1] * k);


        bbx_per_class_data = (float *) nn_tensor_maprw(bbx_per_class);
        if (!bbx_per_class_data) return NN_ERROR_TENSOR_NO_DATA;

        if (!ssd_bbx_decode(bbx_params, bbx_anchor_coords,
                            indices_len_data[nn_tensor_offsetv(indices_len_tensor, 2, k, 0)],
                            bbx_per_class_data)) {
            // pass
        } else
            return NN_ERROR_INTERNAL;

        nn_tensor_unmap(bbx_per_class);

    }


    nn_tensor_unmap(trans);
    nn_tensor_unmap(anchor);
    nn_tensor_unmap(indices_tensor);
    nn_tensor_unmap(indices_len_tensor);
    nn_tensor_release(bbx_per_class);
    nn_tensor_unmap(cache);

    return NN_SUCCESS;
}

NNError nn_ssd_nms_standard(NNTensor *bbx_candidates, NNTensor *indices_len_tensor, NNTensor *iou_threshold,
                            NNTensor *input_img, NNTensor *cache, NNTensor *bbx_out, NNTensor *bbx_out_dim) {

    NNError err;
    const int32_t *indices_len_shape = nn_tensor_shape(indices_len_tensor);
    const int32_t *bbx_candidate_shape = nn_tensor_shape(bbx_candidates);

    float *bbx_candidate_data = (float *) nn_tensor_maprw(bbx_candidates);

    int32_t *bbx_out_dim_data = (int32_t *) nn_tensor_maprw(bbx_out_dim);
    if (!bbx_out_dim_data) return NN_ERROR_TENSOR_NO_DATA;

    float *bbx_out_data = (float *) nn_tensor_maprw(bbx_out);
    if (!bbx_out_data) return NN_ERROR_TENSOR_NO_DATA;

    float *iou_threshold_data = (float *) nn_tensor_mapro(iou_threshold);
    if (!iou_threshold_data) return NN_ERROR_TENSOR_NO_DATA;

    int32_t *indices_len_data = (int32_t *) nn_tensor_maprw(indices_len_tensor);
    if (!indices_len_data) return NN_ERROR_TENSOR_NO_DATA;

    float *cache_data = (float *) nn_tensor_maprw(cache);

    const int32_t *input_shape = nn_tensor_shape(input_img);
    float input_width = (float) input_shape[1];

    int *indices_cache = (int *) cache_data;
    float *area_cache = (float *) ((float *) cache_data + 4 * bbx_candidate_shape[1]);
    float *bbx_cache = (float *) ((float *) cache_data + 8 * bbx_candidate_shape[1]);

    int valid_indices_tmp = 0;
    int valid_indices = 0;

    for (int k = 0; k < indices_len_shape[0]; k++) {
        int out_num = 0;

        valid_indices = indices_len_data[nn_tensor_offsetv(indices_len_tensor, 2, k, 0)];

        for (int i = 0; i < valid_indices; i++) {
            indices_cache[i] = i;
            area_cache[i] = ((bbx_candidate_data[nn_tensor_offsetv(bbx_candidates, 3, k, i, 3)] * input_width) -
                             (bbx_candidate_data[nn_tensor_offsetv(bbx_candidates, 3, k, i, 1)] * input_width) + 1.0) *
                            ((bbx_candidate_data[nn_tensor_offsetv(bbx_candidates, 3, k, i, 2)] * input_width) -
                             (bbx_candidate_data[nn_tensor_offsetv(bbx_candidates, 3, k, i, 0)] * input_width) + 1.0);
            bbx_cache[4 * i] = bbx_candidate_data[nn_tensor_offsetv(bbx_candidates, 3, k, i, 0)] * input_width;
            bbx_cache[4 * i + 1] = bbx_candidate_data[nn_tensor_offsetv(bbx_candidates, 3, k, i, 1)] * input_width;
            bbx_cache[4 * i + 2] = bbx_candidate_data[nn_tensor_offsetv(bbx_candidates, 3, k, i, 2)] * input_width;
            bbx_cache[4 * i + 3] = bbx_candidate_data[nn_tensor_offsetv(bbx_candidates, 3, k, i, 3)] * input_width;
        }

        while (valid_indices > 0) {
            for (int i = 0; i < valid_indices; i++) {
                bbx_cache[4 * i] = bbx_cache[4 * indices_cache[i]];
                bbx_cache[4 * i + 1] = bbx_cache[4 * indices_cache[i] + 1];
                bbx_cache[4 * i + 2] = bbx_cache[4 * indices_cache[i] + 2];
                bbx_cache[4 * i + 3] = bbx_cache[4 * indices_cache[i] + 3];
                area_cache[i] = area_cache[indices_cache[i]];
            }

            bbx_out_data[nn_tensor_offsetv(bbx_out, 3, k, out_num, 0)] = bbx_cache[0];
            bbx_out_data[nn_tensor_offsetv(bbx_out, 3, k, out_num, 1)] = bbx_cache[1];
            bbx_out_data[nn_tensor_offsetv(bbx_out, 3, k, out_num, 2)] = bbx_cache[2];
            bbx_out_data[nn_tensor_offsetv(bbx_out, 3, k, out_num, 3)] = bbx_cache[3];
            out_num++;
            valid_indices_tmp = 0;

            for (int i = 0; i < valid_indices; i++) {
                float yy1 = bbx_cache[0] > bbx_cache[4 * i] ? bbx_cache[0] : bbx_cache[4 * i];
                float xx1 = bbx_cache[1] > bbx_cache[4 * i + 1] ? bbx_cache[1] : bbx_cache[4 * i + 1];
                float yy2 = bbx_cache[2] > bbx_cache[4 * i + 2] ? bbx_cache[2] : bbx_cache[4 * i + 2];
                float xx2 = bbx_cache[3] > bbx_cache[4 * i + 3] ? bbx_cache[3] : bbx_cache[4 * i + 3];

                float ww = (xx2 - xx1 + 1.0) > 0 ? (xx2 - xx1 + 1.0) : 0.0;
                float hh = (yy2 - yy1 + 1.0) > 0 ? (yy2 - yy1 + 1.0) : 0.0;
                float intersection = ww * hh;
                float overlap = intersection / (area_cache[0] + area_cache[i] - intersection);
                if (overlap < iou_threshold_data[0]) {
                    indices_cache[valid_indices_tmp] = i;
                    valid_indices_tmp++;
                }
            }

            valid_indices = valid_indices_tmp;

        }
        bbx_out_dim_data[k] = out_num;
    }

    nn_tensor_unmap(bbx_out);
    nn_tensor_unmap(bbx_out_dim);
    nn_tensor_unmap(iou_threshold);
    nn_tensor_unmap(indices_len_tensor);
    nn_tensor_unmap(bbx_candidates);
    nn_tensor_unmap(cache);

    return NN_SUCCESS;
}

NNError nn_ssd_sort_scores(NNTensor *score_tensor, NNTensor *cache, NNTensor *class_ind_tensor,
                           NNTensor *score_threshold_tensor, NNTensor *max_ind_tensor, NNTensor *indices_tensor,
                           NNTensor *indices_len_tensor) {

    NNError err;
    const int32_t *score_shape = nn_tensor_shape(score_tensor);
    const int32_t *class_ind_shape = nn_tensor_shape(class_ind_tensor);
    const int32_t *max_ind_tensor_val = (int32_t *) nn_tensor_mapro(max_ind_tensor);
    int32_t cache_size = nn_tensor_size(cache);

    int upper_ind = max_ind_tensor_val[0] < score_shape[1] ? max_ind_tensor_val[0] : score_shape[1];
    float *score_data = (float *) nn_tensor_mapro(score_tensor);
    int32_t *class_ind_data = (int32_t *) nn_tensor_mapro(class_ind_tensor);
    float *score_threshold_data = (float *) nn_tensor_mapro(score_threshold_tensor);
    float *cache_data = (float *) nn_tensor_maprw(cache);
    stValInd *val_ind = (stValInd *) cache_data;

    if (cache_size < sizeof(stValInd) * score_shape[1])
        return NN_ERROR_OUT_OF_RESOURCES;

    int32_t *indices_data = (int32_t *) nn_tensor_maprw(indices_tensor);
    if (!indices_data) return NN_ERROR_TENSOR_NO_DATA;

    int32_t *indices_len_data = (int32_t *) nn_tensor_maprw(indices_len_tensor);
    if (!indices_len_data) return NN_ERROR_TENSOR_NO_DATA;

    for (int j = 0; j < class_ind_shape[1]; j++) {
        for (int i = 0; i < score_shape[1]; i++) {
            val_ind[i].val = score_data[nn_tensor_offsetv(score_tensor, 4, 0, i, class_ind_data[j], 0)];
            val_ind[i].id = i;
        }

        qsort(val_ind, score_shape[1], sizeof(stValInd), compare);

        int idx = 0;
        for (int k = 0; k < upper_ind; k++) {
            if (val_ind[score_shape[1] - 1 - k].val > score_threshold_data[j]) {
                indices_data[nn_tensor_offsetv(indices_tensor, 2, j, idx)] = val_ind[score_shape[1] - 1 - k].id;
                idx++;
            }
        }
        indices_len_data[j] = idx;
    }

    nn_tensor_unmap(score_tensor);
    nn_tensor_unmap(max_ind_tensor);
    nn_tensor_unmap(class_ind_tensor);
    nn_tensor_unmap(score_threshold_tensor);
    nn_tensor_unmap(indices_tensor);
    nn_tensor_unmap(indices_len_tensor);
    nn_tensor_unmap(cache);

    return NN_SUCCESS;
}

/**
 * @brief
 *
 * @param bbx_candidates        input of REQ_CLASS_NUM x BBX_NUM x 4
 * @param indices_len_tensor    input of REQ_CLASS_NUM x 1
 * @param iou_threshold         input of 1 x 1
 * @param input_img             input of 1 x w x h x c
 * @param cache
 * @param bbx_out              output of REQ_CLASS_NUM x BBX_NUM x 4
 * @param bbx_out_dim          output of REQ_CLASS_NUM x 1
 *
 * @return
 **/



static NNEngine *engine = NULL;
#define MAX_CLASS_CNT 91

using namespace std::chrono;

int
main(int argc, char **argv) {
    std::string input_filename;
    char model_filename[256];
    int valid_arg = 0;
    int num_candidates = 50;
    int class_id = 1;
    int input_class_id;
    int input_num_candidates;
    float threshold = -0.5;
    float input_threshold;

    float input_nms_threshold = 0;
    float nms_threshold = 0.82;
    int class_id_array[MAX_CLASS_CNT] = {0};
    int class_id_cnt = 0;
    bool limit_classes = false;
    int index = 0;

    static struct option options[] =
            {{"help",      no_argument,       NULL, 'h'},
             {"verbose",   no_argument,       NULL, 'V'},
             {"input",     required_argument, NULL, 'i'},
             {"model",     required_argument, NULL, 'm'},
             {"class",     required_argument, NULL, 'c'},
             {"number",    required_argument, NULL, 'n'},
             {"threshold", required_argument, NULL, 't'},
             {"section",   required_argument, NULL, 's'}};

    if (argc == 1) {
        printf("%s\n", USAGE);
        return 0;
    }

    for (;;) {
        int opt = getopt_long(argc, argv, "hvVi:m:c:n:t:s:", options, NULL);
        if (opt == -1) break;
        switch (opt) {
            case 'h':
                printf(USAGE);
                return 1;
            case 'v':
            case 'V':
                break;
            case 'i':
                input_filename = optarg;
                //strncpy(input_filename, optarg, strlen(optarg) + 1);
                valid_arg += 1;
                break;
            case 'm':
                strncpy(model_filename, optarg, strlen(optarg) + 1);
                valid_arg += 1;
                break;
            case 'c':
                limit_classes = true;
                index = optind - 1;

                while (index < argc) {
                    char *next = strdup(argv[index]);
                    index++;
                    if (next[0] != '-') {
                        input_class_id = atoi(next);
                        if ((input_class_id > 0) && (input_class_id < 91))
                            class_id = input_class_id;
                        else
                            printf(
                                    "Invalid class index, default value of 1 used\r\n");

                        class_id_array[class_id_cnt] = class_id;
                        class_id_cnt++;

                        free(next);
                    } else {
                        free(next);
                        break;
                    }
                }
                optind = index - 1;

                break;
            case 'n':
                input_num_candidates = atoi(optarg);
                if ((input_num_candidates > 0) && (input_num_candidates < 1917))
                    num_candidates = input_num_candidates;
                else
                    printf("Invalid number of candidates, maximum is 1917, default "
                           "value of 50 used\r\n");
                break;
            case 't':
                input_threshold = atof(optarg);
                if ((input_threshold < 1.0))
                    threshold = input_threshold;
                else
                    printf("Invalid score threshold, too high! default value of "
                           "-0.5 used\r\n");
                break;
            case 's':
                input_nms_threshold = atof(optarg);
                if ((input_nms_threshold < 1.0))
                    nms_threshold = input_nms_threshold;
                else
                    printf("Invalid IOU threshold, too high! default value of 0.82 "
                           "used\r\n");
                break;
            default:
                fprintf(stderr,
                        "invalid parameter %c, try --help for usage\n",
                        opt);
                return -1;
        }
    }

    int *detected_object_indices = (int *) calloc(num_candidates, sizeof(int));
    if (!detected_object_indices) {
        fprintf(stderr,
                "Insufficient memory to create %d object indices\n",
                num_candidates);
        return EXIT_FAILURE;
    }

    NNContext *context =
            nn_context_init(NULL, sizeof(pool), pool, sizeof(cache), cache);

    int fd = open(model_filename, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr,
                "failed to open %s: %s\n",
                model_filename,
                strerror(errno));
        return EXIT_FAILURE;
    }

    struct stat st;
    if (fstat(fd, &st)) {
        fprintf(stderr,
                "failed to query %s: %s\n",
                model_filename,
                strerror(errno));
        return EXIT_FAILURE;
    }

    auto start = high_resolution_clock::now();

    const NNModel *model =
            mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (!model || model == MAP_FAILED) {
        fprintf(stderr,
                "failed to map %s: %s\n",
                model_filename,
                strerror(errno));
        return EXIT_FAILURE;
    }

    NNError err = nn_context_model_load(context, st.st_size, model);
    if (err) {
        fprintf(stderr,
                "failed to load model %s: %s\n",
                model_filename,
                nn_strerror(err));
        return EXIT_FAILURE;
    }

    auto model_load = duration_cast<milliseconds>(high_resolution_clock::now() - start);

    fprintf(stderr, "Loading model took '%lld' ms", model_load.count());
    // Use labels embedded in rtm
    if (!limit_classes) {
        class_id_cnt = nn_model_label_count(model);
        for (int i = 0; i < class_id_cnt; i++) {
            class_id_array[i] = i + 1;
        }
    }

    NNTensor *input = nn_context_tensor(context, "input");
    if (!input) {
        fprintf(stderr, "failed to load layer '%s' from model\n", "input");
        return EXIT_FAILURE;
    }

    NNTensor *trans = nn_context_tensor(context, "output1");
    if (!trans) {
        fprintf(stderr, "failed to load layer '%s' from model\n", "trans");
        return EXIT_FAILURE;
    }

    // score tensor
    NNTensor *score_tensor = nn_context_tensor(context, "output2");
    if (!score_tensor) {
        fprintf(stderr,
                "failed to load layer '%s' from model\n",
                "score_tensor");
        return EXIT_FAILURE;
    }
    const int32_t *score_tensor_shape = nn_tensor_shape(score_tensor);

    NNTensor *anchor = nn_context_tensor(context, "ssd_anchor_boxes");
    if (!anchor) {
        fprintf(stderr, "failed to load layer '%s' from model\n", "anchor");
        return EXIT_FAILURE;
    }

    // Model input shape, assuming tensor layout of NHWC (batch, height, width, channels).
    const int32_t *input_shape = nn_tensor_shape(input);
    const int nms_width = input_shape[1];

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //   allocating memory and set values for additional tensors
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // class ID tensor
    char class_id_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *class_id_tensor = nn_tensor_init(class_id_tensor_mem, engine);
    int32_t *data_class_id = (int32_t *) calloc(class_id_cnt, sizeof(int32_t));
    for (int i = 0; i < class_id_cnt; i++) data_class_id[i] = class_id_array[i];

    int32_t shape_class_id[4];
    shape_class_id[0] = 1;
    shape_class_id[1] = class_id_cnt;
    err = nn_tensor_assign(class_id_tensor,
                           NNTensorType_I32,
                           2,
                           shape_class_id,
                           data_class_id);
    if (err)
        fprintf(stderr,
                "failed to assign class_id tensor: %s\n",
                nn_strerror(err));

    // score threshold tensor
    char score_threshold_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *score_threshold_tensor =
            nn_tensor_init(score_threshold_tensor_mem, engine);
    float *data_score_threshold = (float *) calloc(class_id_cnt, sizeof(float));
    for (int i = 0; i < class_id_cnt; i++) data_score_threshold[i] = threshold;
    int32_t shape_score_threshold[4];
    shape_score_threshold[0] = 1;
    shape_score_threshold[1] = class_id_cnt;
    err = nn_tensor_assign(score_threshold_tensor,
                           NNTensorType_F32,
                           2,
                           shape_score_threshold,
                           data_score_threshold);
    if (err)
        fprintf(stderr,
                "failed to assign score_threshold tensor: %s\n",
                nn_strerror(err));

    // IOU threshold
    char iou_threshold_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *iou_threshold_tensor =
            nn_tensor_init(iou_threshold_tensor_mem, engine);
    float *data_iou_threshold = (float *) calloc(1, sizeof(float));
    data_iou_threshold[0] = nms_threshold;
    int32_t shape_iou_threshold[4];
    shape_iou_threshold[0] = 1;
    shape_iou_threshold[1] = 1;
    err = nn_tensor_assign(iou_threshold_tensor,
                           NNTensorType_F32,
                           2,
                           shape_iou_threshold,
                           data_iou_threshold);
    if (err)
        fprintf(stderr,
                "failed to assign score_threshold tensor: %s\n",
                nn_strerror(err));

    // max ind tensor
    char max_id_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *max_id_tensor = nn_tensor_init(max_id_tensor_mem, engine);
    int32_t *data_max_id = (int32_t *) calloc(1, sizeof(int32_t));
    data_max_id[0] = num_candidates;
    int32_t shape_max_id[4];
    shape_max_id[0] = 1;
    shape_max_id[1] = 1;
    err = nn_tensor_assign(max_id_tensor,
                           NNTensorType_I32,
                           2,
                           shape_max_id,
                           data_max_id);
    if (err)
        fprintf(stderr,
                "failed to assign class_id tensor: %s\n",
                nn_strerror(err));

    // indices tensor
    char indices_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *indices_tensor = nn_tensor_init(indices_tensor_mem, engine);
    int32_t *data_indices =
            (int32_t *) calloc(shape_class_id[1] * score_tensor_shape[1],
                               sizeof(int32_t));
    int32_t shape_indices[4];
    shape_indices[0] = shape_class_id[1];
    shape_indices[1] = score_tensor_shape[1];
    err = nn_tensor_assign(indices_tensor,
                           NNTensorType_I32,
                           2,
                           shape_indices,
                           data_indices);
    if (err)
        fprintf(stderr,
                "failed to assign indices tensor: %s\n",
                nn_strerror(err));

    // indices len tensor
    char indices_len_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *indices_len_tensor =
            nn_tensor_init(indices_len_tensor_mem, engine);
    int32_t *data_indices_len =
            (int32_t *) calloc(shape_class_id[1], sizeof(int32_t));
    int32_t shape_indices_len[4];
    shape_indices_len[0] = shape_class_id[1];
    shape_indices_len[1] = 1;
    err = nn_tensor_assign(indices_len_tensor,
                           NNTensorType_I32,
                           2,
                           shape_indices_len,
                           data_indices_len);
    if (err)
        fprintf(stderr,
                "failed to assign indices_len tensor: %s\n",
                nn_strerror(err));

    // bbx candidates tensor
    char bbx_candidates_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *bbx_candidates_tensor =
            nn_tensor_init(bbx_candidates_tensor_mem, engine);
    float *data_bbx_candidates =
            (float *) calloc(4 * shape_class_id[1] * score_tensor_shape[1],
                             sizeof(float));
    int32_t shape_bbx_candidates[4];
    shape_bbx_candidates[0] = shape_class_id[1];
    shape_bbx_candidates[1] = score_tensor_shape[1];
    shape_bbx_candidates[2] = 4;
    err = nn_tensor_assign(bbx_candidates_tensor,
                           NNTensorType_F32,
                           3,
                           shape_bbx_candidates,
                           data_bbx_candidates);
    if (err)
        fprintf(stderr,
                "failed to assign bbx_candidates tensor: %s\n",
                nn_strerror(err));

    // bbx out tensor
    char bbx_out_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *bbx_out_tensor = nn_tensor_init(bbx_out_tensor_mem, engine);
    float *data_bbx_out =
            (float *) calloc(4 * shape_class_id[1] * score_tensor_shape[1],
                             sizeof(float));
    int32_t shape_bbx_out[4];
    shape_bbx_out[0] = shape_class_id[1];
    shape_bbx_out[1] = score_tensor_shape[1];
    shape_bbx_out[2] = 4;
    err = nn_tensor_assign(bbx_out_tensor,
                           NNTensorType_F32,
                           3,
                           shape_bbx_out,
                           data_bbx_out);
    if (err)
        fprintf(stderr,
                "failed to assign bbx_out tensor: %s\n",
                nn_strerror(err));

    // bbx out dim tensor
    char bbx_out_dim_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *bbx_out_dim_tensor =
            nn_tensor_init(bbx_out_dim_tensor_mem, engine);
    int32_t *data_bbx_out_dim =
            (int32_t *) calloc(shape_class_id[1], sizeof(int32_t));
    int32_t shape_bbx_out_dim[4];
    shape_bbx_out_dim[0] = shape_class_id[1];
    shape_bbx_out_dim[1] = 1;
    err = nn_tensor_assign(bbx_out_dim_tensor,
                           NNTensorType_I32,
                           2,
                           shape_bbx_out_dim,
                           data_bbx_out_dim);
    if (err)
        fprintf(stderr,
                "failed to assign indices_len tensor: %s\n",
                nn_strerror(err));

    // cache tensor
    char cache_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *cache_tensor = nn_tensor_init(cache_tensor_mem, engine);
    int32_t max_cache_size = 1024 * 1024;
    float *data_cache = (float *) calloc(max_cache_size, sizeof(float));

    int32_t shape_cache[4];
    shape_cache[0] = 1;
    shape_cache[1] = max_cache_size;

    err = nn_tensor_assign(cache_tensor,
                           NNTensorType_F32,
                           2,
                           shape_cache,
                           data_cache);
    if (err)
        fprintf(stderr,
                "failed to assign tensor_cache: %s\n",
                nn_strerror(err));


    WebcamVideoStream *cap;
    if (input_filename.empty())
        cap = new WebcamVideoStream(0);
    else
        cap = new WebcamVideoStream(input_filename);

    cv::Mat first_image = cap->read();
    std::thread thread_object(&WebcamVideoStream::update, cap);
    cv::Mat image = first_image;

    cv::namedWindow("SSD Detection", cv::WINDOW_NORMAL | cv::WINDOW_KEEPRATIO);
    cv::resizeWindow("SSD Detection", cv::Size(640, 480));
    double deltaTime = 0;
    unsigned int frames = 0;
    double frameRate = 30;
    double averageFrameTimeMilliseconds = 33.333;
    for (;;) {
        auto frames_start = high_resolution_clock::now();
        // Loads the video frame into an OpenCV Mat.
        if (image.empty()) {
            cv::waitKey(0);
            break;
        }
        cv::Mat original_image = image.clone();
        // Converts from the default OpenCV BGR to RGB format of the model (DeepViewRT itself doesn't care).
        cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
        // Resizes the image with default interpolation to the target model size.
        float width_scale = (float) image.cols / input_shape[2];
        float height_scale = (float) image.rows / input_shape[1];
        cv::resize(image, image, cv::Size(input_shape[1], input_shape[2]));

        // Memory maps the input tensor,.
        auto input_map = nn_tensor_maprw(input);
        if (!input_map) { printf("[ERROR] no memory for input tensor\n"); }
        // Create an OpenCV Mat with memory backed by the input tensor's memory map.
        cv::Mat input_mat(input_shape[1], input_shape[2], CV_32FC3, input_map);
        // Convert's the CV_8UC3 image to CV_32FC3 (3-channel, 32bit floating-point).
        image.convertTo(input_mat, CV_32FC3);
        // Perform input normalization using x = (x - 127) / 128
        input_mat = (input_mat - 128.0f) / 128.0f;

        start = high_resolution_clock::now();
        err = nn_context_run(context);
        if (err) {
            fprintf(stderr, "failed to run model: %s\n", nn_strerror(err));
            return EXIT_FAILURE;
        }
        auto inference_time = duration_cast<milliseconds>(high_resolution_clock::now() - start);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //      compute the bounding box
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        start = high_resolution_clock::now();
        err = nn_ssd_sort_scores(score_tensor,
                                 cache_tensor,
                                 class_id_tensor,
                                 score_threshold_tensor,
                                 max_id_tensor,
                                 indices_tensor,
                                 indices_len_tensor);

        err = nn_ssd_decode_bounding_box(trans,
                                         anchor,
                                         cache_tensor,
                                         indices_tensor,
                                         indices_len_tensor,
                                         bbx_candidates_tensor);

        err = nn_ssd_nms_standard(bbx_candidates_tensor,
                                  indices_len_tensor,
                                  iou_threshold_tensor,
                                  input,
                                  cache_tensor,
                                  bbx_out_tensor,
                                  bbx_out_dim_tensor);

        auto post_process_time = duration_cast<milliseconds>(high_resolution_clock::now() - start);
        err = nn_softmax(score_tensor, score_tensor);
        float *score_data = (float *) nn_tensor_maprw(score_tensor);
        int32_t *indices_data = (int32_t *) nn_tensor_maprw(indices_tensor);
        for (int k = 0; k < class_id_cnt; k++) {
            if (data_bbx_out_dim[k] > 0) {
                //printf("\t Class ID = [%d]\r\n", class_id_array[k]);
                std::string label(nn_model_label(model, k));

                for (int i = 0; i < data_bbx_out_dim[k]; i++) {

                    auto ymin = data_bbx_out[nn_tensor_offsetv(bbx_out_tensor, 3, k, i, 0)] * (float) height_scale;
                    auto xmin = data_bbx_out[nn_tensor_offsetv(bbx_out_tensor, 3, k, i, 1)] * (float) width_scale;
                    auto ymax = data_bbx_out[nn_tensor_offsetv(bbx_out_tensor, 3, k, i, 2)] * (float) height_scale;
                    auto xmax = data_bbx_out[nn_tensor_offsetv(bbx_out_tensor, 3, k, i, 3)] * (float) width_scale;
                    auto indices = indices_data[nn_tensor_offsetv(indices_tensor, 2, k, 0)];
                    auto score = score_data[nn_tensor_offsetv(score_tensor, 4, 0, indices, class_id_array[k], 0)];
                    cv::rectangle(original_image, cv::Point(xmin, ymin),
                                  cv::Point(xmax, ymax),
                                  cv::Scalar(0, 255, 0), 4);
                    cv::putText(original_image, label.append(": " + std::to_string(score)),
                                cv::Point(xmin + 20, ymin + 40),
                                cv::FONT_HERSHEY_SIMPLEX, 1,
                                cv::Scalar(255, 0, 255), 2);
//                    printf("\tPredicted bounding box[%d]: %.3f %.3f %.3f %.3f\r\n score: %.3f\n",
//                           i,
//                           ymin,
//                           xmin,
//                           ymax,
//                           xmax,
//                           score);
                }
            }
        }
        printf("inference takes %lld ms \npost processing takes %lld ms\r\n",
               inference_time.count(),
               post_process_time.count());

        image = cap->read();
        deltaTime += duration_cast<milliseconds>(high_resolution_clock::now() - frames_start).count();
        frames++;
        if (deltaTime > 1000.0) {
            frameRate = frames;
            frames = 0;
            deltaTime -= CLOCKS_PER_SEC;
            averageFrameTimeMilliseconds = 1000.0 / (frameRate == 0 ? 0.001 : frameRate);
        }

        cv::putText(original_image, cv::format("%.2f", frameRate),
                    cv::Point(original_image.cols - 50, 20),
                    cv::FONT_HERSHEY_SIMPLEX, 0.5,
                    cv::Scalar(0, 255, 0));

        cv::imshow("SSD Detection", original_image);
        if ((cv::waitKey(1) & 0xFF) == 113)
            break;
    }
    cap->release();
    thread_object.join();
    cv::destroyAllWindows();
    std::cout << "Average Frame Time was " << averageFrameTimeMilliseconds << " ms" << std::endl;

    nn_context_release(context);
    munmap((void *) model, st.st_size);
    close(fd);

    free(data_class_id);
    nn_tensor_release(class_id_tensor);
    free(data_score_threshold);
    nn_tensor_release(score_threshold_tensor);
    free(data_iou_threshold);
    nn_tensor_release(iou_threshold_tensor);
    free(data_max_id);
    nn_tensor_release(max_id_tensor);
    free(data_indices);
    nn_tensor_release(indices_tensor);
    free(data_indices_len);
    nn_tensor_release(indices_len_tensor);
    free(data_bbx_candidates);
    nn_tensor_release(bbx_candidates_tensor);
    free(data_bbx_out);
    nn_tensor_release(bbx_out_tensor);
    free(data_bbx_out_dim);
    nn_tensor_release(bbx_out_dim_tensor);
    free(data_cache);
    nn_tensor_release(cache_tensor);

    return EXIT_SUCCESS;
}
