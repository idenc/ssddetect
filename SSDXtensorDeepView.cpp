// this program implements the detection using a SSD RTM
#include <chrono>
#include <cerrno>
#include <fcntl.h>
#include <cstdlib>
#include <cstring>
#include <sys/stat.h>
#include <iostream>
#include <stdio.h>
#include "xtensor/xarray.hpp"
#include "xtensor/xbuilder.hpp"
#include "xtensor/xio.hpp"
#include "xtensor/xview.hpp"
#include "xtensor/xindex_view.hpp"
#include "xtensor/xstrided_view.hpp"
#include "xtensor/xadapt.hpp"
#include "xtensor/xmasked_view.hpp"
#include "xtensor/xsort.hpp"

/*
 * POSIX MMAP for Windows, optional but this example is written to memory map
 * the RT Models as opposed to reading them directly into memory.
 */
#ifdef _WIN32

#include <Windows.h>
#include <io.h>
#include "mman.h"

#else

#include <sys/mman.h>
#include <unistd.h>

#endif

#include "deepview_rt.h"
#include "getopt.h"

#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include "WebcamVideoStream.h"

#define USAGE \
    "\
\n\
USAGE: \n\
       detect [hvV] \n\
       detect [-m mobilenetSSD_rtm_file] [-i sample image] <-c class_id> <-t threshold> <-n num_boxes>\n\
       -h, --help\n\
          \tDisplay help information\n\
       -v, --version\n\
       -V, --verbose\n\
          \tDisplay the verbose information\n\
          \tDisplay version information\n\
       -i, --input\n\
          \tInput testing image file, leave blank to use webcam \n\
       -m, --model\n\
          \tInput RTM model\n\
       -t, --threshold \n\
          \tThreshold for valid scores, by default it is set to 0.5\n\
       -s, --section \n\
          \tIOU threshold for NMS, by default it is set to 0.5\n\
       -c, --class \n\
          \tClass indices (model specific, i.e.,COCO dataset labels 1-91) \n\
       -n, --number \n\
          \tNumer of maximum predictions (bounding boxes)\n\
\n\
       \e[4mSample command:\e[0m\n\
       ./detect -m DATA_PATH/ssd_model.rtm -i DATA_PATH/sample.png -c 1 -s 0.82 \n\
\n\
       \e[4mSample command:\e[0m\n\
       ./detect -m DATA_PATH/ssd_model.rtm -i DATA_PATH/sample.png -c 1 18 -s 0.9 \n\
\n\
       \e[4mNotes:\e[0m\n\
       1. Sample data is located in smb://auzonenas01.local/share/Zhe/data\n\
       2. Output bounding box parameters are [ymin, xmin, ymax, xmax] normalized by input image width/height\n\
\n\
"

#ifdef _MSC_VER
__declspec(align(64)) static uint8_t cache[72 * 1024 * 1024];
__declspec(align(64)) static uint8_t pool[72 * 1024 * 1024];
#else
static uint8_t cache[4 * 1024 * 1024] __attribute((aligned(64)));
static uint8_t pool[32 * 1024 * 1024] __attribute((aligned(64)));
#endif

static NNEngine *engine = NULL;
#define MAX_CLASS_CNT 91

using namespace std;
using namespace chrono;
using namespace xt::placeholders;

/**
 * @ingroup logical_operators
 * @brief return vector of row indices where arr is not zero
 *
 * @param arr input array
 * @return vector of size_types where arr is not equal to zero
 */
template<class T>
inline auto rowwhere(const T &arr) {
    auto shape = arr.shape();
    using index_type = xt::xindex_type_t<typename T::shape_type>;
    using size_type = typename T::size_type;

    auto idx = xtl::make_sequence<index_type>(arr.dimension(), 0);
    std::vector<size_type> indices;

    size_type total_size = xt::compute_size(shape);
    for (size_type i = 0; i < total_size; i++, xt::detail::next_idx(shape, idx)) {
        if (arr.element(std::begin(idx), std::end(idx))) {
            indices.push_back(i);
        }
    }

    return indices;
}

/**
 * @brief creates a view into \a e filtered by \a condition.
 *
 * Returns an xt::view (2D view) with the rows selected where \a condition evaluates to \em true.
 *
 * @param e the underlying xexpression
 * @param condition xexpression with shape of \a e which selects rows
 *
 * \code{.cpp}
 * xarray<int> a = { { 0, 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9, 10, 11 } };
 * auto filter = xt::filter_rows(a, xt::view(a, xt::all(), 0) > 4);
 * std::cout << filter << std:endl; // { {8, 9, 10, 11} }
 * \endcode
 */
template<class E, class O>
inline auto filter_rows(E &&e, O &&condition) noexcept {
    auto row_indices = rowwhere(std::forward<O>(condition));
    return view(e, xt::keep(row_indices), xt::all());
}

/**
 * Compute the areas of rectangles given two corners
 * @param left_top shape (N, 2) left top corner
 * @param right_bottom (N, 2) right bottom corner
 * @return area (N) area of each rectangle
 */
xt::xtensor<float, 1> area_of(xt::xtensor<float, 2> left_top, xt::xtensor<float, 2> right_bottom) {
    xt::xtensor<float, 2> sub = right_bottom - left_top;
    xt::xtensor<float, 2> hw = xt::clip(sub, 0.0, xt::amax(sub));
    return (xt::view(hw, xt::all(), 0) * xt::view(hw, xt::all(), 1));
}

/**
 * Return intersection-over-union of boxes
 * @param boxes0 [NUM_BBX, 4] Ground truth boxes
 * @param boxes1 [NUM_BBX, 4] Predicted boxes
 * @param eps A small number to avoid dividing by zero
 * @return [N] IoU values
 */
xt::xtensor<float, 1> iou_of(xt::xtensor<float, 2> boxes0, xt::xtensor<float, 2> boxes1, double eps) {
    xt::xtensor<float, 2> boxes0_first_two = xt::strided_view(boxes0, {xt::ellipsis(), xt::range(_, 2)});
    xt::xtensor<float, 2> boxes0_last_two = xt::strided_view(boxes0, {xt::ellipsis(), xt::range(2, _)});

    xt::xtensor<float, 2> boxes1_first_two = xt::strided_view(boxes1, {xt::ellipsis(), xt::range(_, 2)});
    xt::xtensor<float, 2> boxes1_last_two = xt::strided_view(boxes1, {xt::ellipsis(), xt::range(2, _)});

    xt::xtensor<float, 2> overlap_left_top = xt::maximum(boxes0_first_two, boxes1_first_two);
    xt::xtensor<float, 2> overlap_right_bottom = xt::minimum(boxes0_last_two, boxes1_last_two);

    xt::xtensor<float, 1> overlap_area = area_of(overlap_left_top, overlap_right_bottom);
    xt::xtensor<float, 1> area0 = area_of(boxes0_first_two, boxes0_last_two);
    xt::xtensor<float, 1> area1 = area_of(boxes1_first_two, boxes1_last_two);
    return (overlap_area / (area0 + area1 - overlap_area + eps));
}

/**
 *
 * @param box_probs Tensor of shape [NUM_BBX, 5] holding box coords in corner-form and probabilities
 * @param iou_threshold intersection over union threshold
 * @param top_k Keep top k results. If k <= 0, keep all the results. Default is -1 (keep all)
 * @param candidate_size Only consider this amount of candidates with the highest scores
 * @return A view of kept boxes
 */
xt::xtensor<float, 2> nms(xt::xtensor<float, 2> box_probs, double iou_threshold, int top_k, int candidate_size) {
    xt::xtensor<float, 1> scores = xt::view(box_probs, xt::all(), box_probs.shape()[1] - 1);
    xt::xtensor<float, 2> boxes = xt::view(box_probs,
                                           xt::all(), xt::range(_, box_probs.shape()[1] - 1));

    vector<int> picked;
    xt::xtensor<int, 1> indexes = xt::argsort(scores);

    indexes = xt::view(indexes, xt::range(-1 * candidate_size, _));
    while (indexes.shape()[0] > 0) {
        int current = indexes[indexes.shape()[0] - 1];
        picked.push_back(current);

        if (0 < top_k == picked.size() || indexes.shape()[0] == 1)
            break;

        xt::xtensor<float, 1> current_box = xt::view(boxes, current, xt::all());
        indexes = xt::view(indexes, xt::range(_, indexes.shape()[0] - 1));
        xt::xtensor<float, 2> rest_boxes = xt::view(boxes, xt::keep(xt::eval(indexes)), xt::all());
        xt::xtensor<float, 1> iou = iou_of(rest_boxes, xt::expand_dims(current_box, 0), 1.0e-5);
        indexes = xt::filter(indexes, iou <= iou_threshold);
    }

    return xt::view(box_probs, xt::keep(picked), xt::all());
}


/**
 * @param scores softmax score output from SSD model of shape [1, NUM_PRIORS, NUM_CLASSES]
 * @param boxes Decoded bbx output from SSD model of shape [1, NUM_PRIORS, 4]
 * bbox have format [xmin, ymin, xmax, ymax]
 * @param bbx_out Output tensor of shape [NUM_CLASSES, BBX_NUM, 4] that final bbx coords are written to
 * @param num_bbx_per_class Output tensor of shape [NUM_CLASSES, 1] that holds number of bbxs for each class
 * @param bbx_probs Output tensor of shape [NUM_CLASSES, BBX_NUM] that holds confidence scores for each bbx
 * @param top_k Keep top k results. If k <= 0, keep all the results. Default is -1 (keep all)
 * @param prob_threshold Threshold for softmax probs. Default is 0.5
 * @param iou_threshold Threshold for IOU during NMS. Default is 0.5
 * @param candidate_size Only consider this amount of candidates with the highest scores
 * @return tuple containing (picked_bboxes, picked_labels, picked_bbox_probs)
 **/
auto decodeSSDPredictions(NNTensor *scores,
                          NNTensor *boxes,
                          NNTensor *bbx_out,
                          NNTensor *num_bbx_per_class,
                          NNTensor *bbx_probs,
                          int top_k,
                          double prob_threshold,
                          double iou_threshold,
                          int candidate_size) {
    // Map scores to xtensor
    const int32_t *score_shape = nn_tensor_shape(scores);
    auto scores_result = (float *) nn_tensor_mapro(scores);
    if (!scores_result) return NN_ERROR_TENSOR_NO_DATA;
    size_t size_scores = nn_tensor_volume(scores);
    vector<int32_t> shape_scores = {score_shape[1], score_shape[2]};
    xt::xtensor<float, 2> scoresXtensor = xt::adapt(
            scores_result, size_scores, xt::no_ownership(), shape_scores);

    // Map boxes to xtensor
    const int32_t *box_shape = nn_tensor_shape(boxes);
    auto box_result = (float *) nn_tensor_mapro(boxes);
    if (!box_result) return NN_ERROR_TENSOR_NO_DATA;
    size_t size_boxes = nn_tensor_volume(boxes);
    vector<int32_t> shape_boxes = {box_shape[1], box_shape[2]};
    xt::xtensor<float, 2> boxesXtensor = xt::adapt(
            box_result, size_boxes, xt::no_ownership(), shape_boxes);

    float *bbx_out_data = (float *) nn_tensor_maprw(bbx_out);
    if (!bbx_out_data) return NN_ERROR_TENSOR_NO_DATA;

    int32_t *num_bbx_per_class_data = (int32_t *) nn_tensor_maprw(num_bbx_per_class);
    if (!num_bbx_per_class_data) return NN_ERROR_TENSOR_NO_DATA;

    float *bbx_probs_data = (float *) nn_tensor_maprw(bbx_probs);
    if (!bbx_probs_data) return NN_ERROR_TENSOR_NO_DATA;

    // For each class (0 is background)
    for (int class_index = 1; class_index < score_shape[2]; class_index++) {
        xt::xarray<float> probs = xt::view(scoresXtensor, xt::all(), class_index);
        // Filter probabilities to only those greater than prob_threshold
        xt::xtensor<bool, 1> mask = probs > prob_threshold;
        probs = xt::filter(probs, mask);
        // If no probabilities greater than prob_threshold continue to next class
        if (probs.shape()[0] == 0)
            continue;
        // Filter box coords
        xt::xtensor<float, 2> subset_boxes = filter_rows(boxesXtensor, mask);
        probs.reshape({-1, 1});
        xt::xtensor<float, 2> box_probs = xt::concatenate(xt::xtuple(subset_boxes, probs), 1);
        // Perform NMS
        box_probs = nms(box_probs, iou_threshold, top_k, candidate_size);

        // Assign coordinates and probs to NNTensors
        for (int i = 0; i < box_probs.shape()[0]; i++) {
            auto bbx_coords = xt::view(box_probs, i, xt::range(_, 4));
            float bbx_prob = xt::view(box_probs, i, 4);
            bbx_out_data[nn_tensor_offsetv(bbx_out, 3, class_index, i, 0)] = bbx_coords[0];
            bbx_out_data[nn_tensor_offsetv(bbx_out, 3, class_index, i, 1)] = bbx_coords[1];
            bbx_out_data[nn_tensor_offsetv(bbx_out, 3, class_index, i, 2)] = bbx_coords[2];
            bbx_out_data[nn_tensor_offsetv(bbx_out, 3, class_index, i, 3)] = bbx_coords[3];
            bbx_probs_data[nn_tensor_offsetv(bbx_probs, 2, class_index, i)] = bbx_prob;
        }
        // Keep track of number of bounding boxes per class
        num_bbx_per_class_data[class_index] = box_probs.shape()[0];
    }

    nn_tensor_unmap(scores);
    nn_tensor_unmap(boxes);
    nn_tensor_unmap(bbx_out);
    nn_tensor_unmap(num_bbx_per_class);
    nn_tensor_unmap(bbx_probs);

    return NN_SUCCESS;
}

int
main(int argc, char **argv) {
    std::string input_filename;
    char model_filename[256];
    int valid_arg = 0;
    int num_candidates = 200;
    int class_id = 1;
    int input_class_id;
    int input_num_candidates;
    float threshold = 0.5;
    float input_threshold;

    float input_nms_threshold = 0;
    float nms_threshold = 0.5;
    int class_id_array[MAX_CLASS_CNT] = {0};
    int class_id_cnt = 0;
    bool limit_classes = false;
    int index = 0;

    static struct option options[] =
            {{"help",      no_argument,       NULL, 'h'},
             {"verbose",   no_argument,       NULL, 'V'},
             {"input",     required_argument, NULL, 'i'},
             {"model",     required_argument, NULL, 'm'},
             {"class",     required_argument, NULL, 'c'},
             {"number",    required_argument, NULL, 'n'},
             {"threshold", required_argument, NULL, 't'},
             {"section",   required_argument, NULL, 's'}};

    if (argc == 1) {
        printf("%s\n", USAGE);
        return 0;
    }

    for (;;) {
        int opt = getopt_long(argc, argv, "hvVi:m:c:n:t:s:", options, NULL);
        if (opt == -1) break;
        switch (opt) {
            case 'h':
                printf(USAGE);
                return 1;
            case 'v':
            case 'V':
                break;
            case 'i':
                input_filename = optarg;
                valid_arg += 1;
                break;
            case 'm':
                strncpy(model_filename, optarg, strlen(optarg) + 1);
                valid_arg += 1;
                break;
            case 'c':
                limit_classes = true;
                index = optind - 1;

                while (index < argc) {
                    char *next = strdup(argv[index]);
                    index++;
                    if (next[0] != '-') {
                        input_class_id = atoi(next);
                        if ((input_class_id > 0) && (input_class_id < 91))
                            class_id = input_class_id;
                        else
                            printf(
                                    "Invalid class index, default value of 1 used\r\n");

                        class_id_array[class_id_cnt] = class_id;
                        class_id_cnt++;

                        free(next);
                    } else {
                        free(next);
                        break;
                    }
                }
                optind = index - 1;

                break;
            case 'n':
                input_num_candidates = atoi(optarg);
                if ((input_num_candidates > 0) && (input_num_candidates < 1917))
                    num_candidates = input_num_candidates;
                else
                    printf("Invalid number of candidates, maximum is 1917, default "
                           "value of 50 used\r\n");
                break;
            case 't':
                input_threshold = atof(optarg);
                if ((input_threshold <= 1.0))
                    threshold = input_threshold;
                else
                    printf("Invalid score threshold, too high! default value of "
                           "0.5 used\r\n");
                break;
            case 's':
                input_nms_threshold = atof(optarg);
                if ((input_nms_threshold <= 1.0))
                    nms_threshold = input_nms_threshold;
                else
                    printf("Invalid IOU threshold, too high! default value of 0.5 "
                           "used\r\n");
                break;
            default:
                fprintf(stderr,
                        "invalid parameter %c, try --help for usage\n",
                        opt);
                return -1;
        }
    }

    int *detected_object_indices = (int *) calloc(num_candidates, sizeof(int));
    if (!detected_object_indices) {
        fprintf(stderr,
                "Insufficient memory to create %d object indices\n",
                num_candidates);
        return EXIT_FAILURE;
    }

    NNContext *context =
            nn_context_init(NULL, sizeof(pool), pool, sizeof(cache), cache);

    int fd = open(model_filename, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr,
                "failed to open %s: %s\n",
                model_filename,
                strerror(errno));
        return EXIT_FAILURE;
    }

    struct stat st;
    if (fstat(fd, &st)) {
        fprintf(stderr,
                "failed to query %s: %s\n",
                model_filename,
                strerror(errno));
        return EXIT_FAILURE;
    }

    auto start = high_resolution_clock::now();

    const NNModel *model =
            mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (!model || model == MAP_FAILED) {
        fprintf(stderr,
                "failed to map %s: %s\n",
                model_filename,
                strerror(errno));
        return EXIT_FAILURE;
    }

    NNError err = nn_context_model_load(context, st.st_size, model);
    if (err) {
        fprintf(stderr,
                "failed to load model %s: %s\n",
                model_filename,
                nn_strerror(err));
        return EXIT_FAILURE;
    }

    auto model_load = duration_cast<milliseconds>(high_resolution_clock::now() - start);

    fprintf(stderr, "Loading model took '%lld' ms\n", model_load.count());
    // Use labels embedded in rtm
    if (!limit_classes) {
        class_id_cnt = nn_model_label_count(model);
        for (int i = 0; i < class_id_cnt; i++) {
            class_id_array[i] = i + 1;
        }
    }

    NNTensor *input = nn_context_tensor(context, "input");
    if (!input) {
        fprintf(stderr, "failed to load layer '%s' from model\n", "input");
        return EXIT_FAILURE;
    }

    // score tensor
    NNTensor *score_tensor = nn_context_tensor(context, "output1");
    if (!score_tensor) {
        fprintf(stderr,
                "failed to load layer '%s' from model\n",
                "score_tensor");
        return EXIT_FAILURE;
    }

    NNTensor *box_tensor = nn_context_tensor(context, "output2");
    if (!box_tensor) {
        fprintf(stderr, "failed to load layer '%s' from model\n", "box_tensor");
        return EXIT_FAILURE;
    }

    const int32_t *score_tensor_shape = nn_tensor_shape(score_tensor);

    // Model input shape, assuming tensor layout of NHWC (batch, height, width, channels).
    const int32_t *input_shape = nn_tensor_shape(input);

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //   allocating memory and set values for additional tensors
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // class ID shape
    int32_t shape_class_id[4];
    shape_class_id[0] = 1;
    shape_class_id[1] = class_id_cnt;

    // bbx out tensor
    char bbx_out_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *bbx_out_tensor = nn_tensor_init(bbx_out_tensor_mem, engine);
    float *data_bbx_out =
            (float *) calloc(4 * shape_class_id[1] * score_tensor_shape[1],
                             sizeof(float));
    int32_t shape_bbx_out[4];
    shape_bbx_out[0] = shape_class_id[1];
    shape_bbx_out[1] = score_tensor_shape[1];
    shape_bbx_out[2] = 4;
    err = nn_tensor_assign(bbx_out_tensor,
                           NNTensorType_F32,
                           3,
                           shape_bbx_out,
                           data_bbx_out);
    if (err)
        fprintf(stderr,
                "failed to assign bbx_out tensor: %s\n",
                nn_strerror(err));

    // bbx probs tensor
    char bbx_probs_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *bbx_probs_tensor = nn_tensor_init(bbx_probs_tensor_mem, engine);
    float *data_bbx_probs =
            (float *) calloc(shape_class_id[1] * score_tensor_shape[1],
                             sizeof(float));
    int32_t shape_bbx_probs[4];
    shape_bbx_probs[0] = shape_class_id[1];
    shape_bbx_probs[1] = score_tensor_shape[1];
    err = nn_tensor_assign(bbx_probs_tensor,
                           NNTensorType_F32,
                           2,
                           shape_bbx_probs,
                           data_bbx_probs);
    if (err)
        fprintf(stderr,
                "failed to assign bbx_probs tensor: %s\n",
                nn_strerror(err));

    // bbx out dim tensor
    char bbx_out_dim_tensor_mem[NN_TENSOR_SIZEOF];
    NNTensor *num_bbx_per_class_tensor =
            nn_tensor_init(bbx_out_dim_tensor_mem, engine);
    int32_t *data_num_bbx_per_class =
            (int32_t *) calloc(shape_class_id[1], sizeof(int32_t));
    int32_t shape_bbx_out_dim[4];
    shape_bbx_out_dim[0] = shape_class_id[1];
    shape_bbx_out_dim[1] = 1;
    err = nn_tensor_assign(num_bbx_per_class_tensor,
                           NNTensorType_I32,
                           2,
                           shape_bbx_out_dim,
                           data_num_bbx_per_class);
    if (err)
        fprintf(stderr,
                "failed to assign num_bbx_per_class tensor: %s\n",
                nn_strerror(err));

    // Memory maps the input tensor,.
    auto input_map = nn_tensor_maprw(input);
    if (!input_map) { printf("[ERROR] no memory for input tensor\n"); }

    WebcamVideoStream *cap;
    if (input_filename.empty())
        cap = new WebcamVideoStream(0);
    else
        cap = new WebcamVideoStream(input_filename);

    cv::Mat first_image = cap->read();
    std::thread thread_object(&WebcamVideoStream::update, cap);
    cv::Mat image = first_image;

    cv::namedWindow("SSD Detection", cv::WINDOW_NORMAL);
    cv::resizeWindow("SSD Detection", cv::Size(640, 480));
    double deltaTime = 0;
    unsigned int frames = 0;
    double averageInferenceTimeMilliseconds = 0;
    double inferenceCounter = 0;
    double averageFrameTimeMilliseconds = 33.333;
    for (;;) {
        auto frames_start = high_resolution_clock::now();
        // Loads the video frame into an OpenCV Mat.
        if (image.empty()) {
            cout << "No more frames to read. Pausing... (Press Q to quit)" << endl;
            cv::waitKey(0);
            break;
        }
        cv::Mat original_image = image.clone();
        // Converts from the default OpenCV BGR to RGB format of the model (DeepViewRT itself doesn't care).
        cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
        // Resizes the image with default interpolation to the target model size.
        cv::resize(image, image, cv::Size(input_shape[1], input_shape[2]));

        // Create an OpenCV Mat with memory backed by the input tensor's memory map.
        cv::Mat input_mat(input_shape[1], input_shape[2], CV_32FC3, input_map);
        // Convert's the CV_8UC3 image to CV_32FC3 (3-channel, 32bit floating-point).
        image.convertTo(input_mat, CV_32FC3);
        // Perform input normalization using x = (x - 127) / 128
        input_mat = (input_mat - 127.0f) / 128.0f;

        start = high_resolution_clock::now();
        err = nn_context_run(context);
        if (err) {
            fprintf(stderr, "failed to run model: %s\n", nn_strerror(err));
            return EXIT_FAILURE;
        }
        auto inference_time = duration_cast<milliseconds>(high_resolution_clock::now() - start);
        inferenceCounter += inference_time.count();

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //      compute the bounding box
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        start = high_resolution_clock::now();
        err = decodeSSDPredictions(score_tensor,
                                   box_tensor,
                                   bbx_out_tensor,
                                   num_bbx_per_class_tensor,
                                   bbx_probs_tensor,
                                   -1,
                                   threshold,
                                   nms_threshold,
                                   num_candidates);
        if (err) {
            fprintf(stderr, "failed to decode SSD predictions: %s\n", nn_strerror(err));
            return EXIT_FAILURE;
        }

        auto post_process_time = duration_cast<milliseconds>(high_resolution_clock::now() - start);

        // For each class (0 is background)
        for (int k = 1; k < class_id_cnt; k++) {
            // Bounding box exists
            if (data_num_bbx_per_class[k] > 0) {
                std::string label(nn_model_label(model, k));
                // For each bounding box assigned to this class
                for (int i = 0; i < data_num_bbx_per_class[k]; i++) {
                    auto xmin =
                            data_bbx_out[nn_tensor_offsetv(bbx_out_tensor, 3, k, i, 0)] * (float) original_image.cols;
                    auto ymin =
                            data_bbx_out[nn_tensor_offsetv(bbx_out_tensor, 3, k, i, 1)] * (float) original_image.rows;
                    auto xmax =
                            data_bbx_out[nn_tensor_offsetv(bbx_out_tensor, 3, k, i, 2)] * (float) original_image.cols;
                    auto ymax =
                            data_bbx_out[nn_tensor_offsetv(bbx_out_tensor, 3, k, i, 3)] * (float) original_image.rows;
                    float score = data_bbx_probs[nn_tensor_offsetv(bbx_probs_tensor, 2, k, i)];
                    cv::rectangle(original_image, cv::Point(xmin, ymin),
                                  cv::Point(xmax, ymax),
                                  cv::Scalar(0, 255, 0), 4);
                    string s(16, '\0');
                    auto rounded = snprintf(&s[0], s.size(), "%.2f", score);
                    s.resize(rounded);
                    cv::putText(original_image, label.append(": " + s),
                                cv::Point(xmin + 20, ymin + 40),
                                cv::FONT_HERSHEY_SIMPLEX, 1,
                                cv::Scalar(255, 0, 255), 2);
                }
            }
        }
        printf("inference takes %lld ms \npost processing takes %lld ms\r\n",
               inference_time.count(),
               post_process_time.count());

        image = cap->read();
        deltaTime += duration_cast<milliseconds>(high_resolution_clock::now() - frames_start).count();
        frames++;
        if (deltaTime > 1000.0) {
            averageFrameTimeMilliseconds = deltaTime / (frames == 0 ? 0.001 : frames);
            deltaTime = 0;
            averageInferenceTimeMilliseconds = inferenceCounter / (frames == 0 ? 0.001 : frames);
            inferenceCounter = 0;
            frames = 0;
        }

        cv::putText(original_image, cv::format("Avg Inference time: %.2f ms", averageInferenceTimeMilliseconds),
                    cv::Point(original_image.cols - 200, 20),
                    cv::FONT_HERSHEY_SIMPLEX, 1.0,
                    cv::Scalar(0, 255, 0), 3);

        cv::putText(original_image, cv::format("Avg Frame time: %.2f ms", averageFrameTimeMilliseconds),
                    cv::Point(original_image.cols - 200, 40),
                    cv::FONT_HERSHEY_SIMPLEX, 1.0,
                    cv::Scalar(0, 255, 0), 3);

        cv::imshow("SSD Detection", original_image);
        // Hit Q to quit
        if ((cv::waitKey(1) & 0xFF) == 113)
            break;
        // Clear tensors for next loop
        err = nn_tensor_fill(bbx_out_tensor, 0.0);
        if (err)
            cout << "Error clearing bbx_out_tensor: " << err << endl;
        err = nn_tensor_fill(bbx_probs_tensor, 0.0);
        if (err)
            cout << "Error clearing bbx_probs_tensor: " << err << endl;
        err = nn_tensor_fill(num_bbx_per_class_tensor, 0.0);
        if (err)
            cout << "Error clearing num_bbx_per_class_tensor: " << err << endl;
    }
    cap->release();
    thread_object.join();
    cv::destroyAllWindows();
    image.release();
    std::cout << "Average Frame Time was " << averageFrameTimeMilliseconds << " ms" << std::endl;

    nn_context_release(context);
    munmap((void *) model, st.st_size);
    close(fd);

    nn_tensor_unmap(input);
    free(data_bbx_out);
    nn_tensor_release(bbx_out_tensor);
    free(data_num_bbx_per_class);
    nn_tensor_release(num_bbx_per_class_tensor);
    free(data_bbx_probs);
    nn_tensor_release(bbx_probs_tensor);

    return EXIT_SUCCESS;
}