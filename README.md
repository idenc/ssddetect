Repo for running video Single Shot Detection. 
TFSSD.cpp is for running trimmed Tensorflow Object Detection API models. 
SSDXtensor.cpp is for running my models with post-processing code written with XTensor.
